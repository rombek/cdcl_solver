// This example illustrates reasoning about Boolean
// formula using Tseitin's encoding and brute force
//

#include <parsing/ast.h>
#include <sat/skeleton.h>
#include <sat/cnf_clause_list.h>

#include "cdcl.h"


int main(int argc, char **argv) {
    //get file name
    if (argc != 2) {
        std::cerr << "expected file name as argument" << std::endl;
        return 1;
    }

    ast::astt ast;

    // undeclared variables are predicates
    ast.set_default_id_token(N_PREDICATE);

    if (ast.parse(argv[1])) {
        std::cerr << "parsing failed" << std::endl;
        return 2;
    }

    // get formula
    ast::nodet f = ast.get_formula();

    if (f.is_nil()) {
        std::cout << "No formula" << std::endl;
        return 3;
    }

    cnf_clause_listt cnfs;
    // make Tseitin's encoding
    skeleton(f, cnfs);

    // get set of clauses
    cnf_clause_listt::clausest clauses = cnfs.get_clauses();
    // write the resultin formula
    for (cnf_clause_listt::clausest::iterator i = clauses.begin(); i != clauses.end();) {
        // write the clause
        bvt clause = *i;
        std::cout << "(";
        for (bvt::iterator j = clause.begin(); j != clause.end();) {
            // write the literal
            literalt l = *j;
            int number = l.dimacs();
            if (number < 0) {
                std::cout << "~";
                number = -number;
            }
            std::cout << "x" << number;
            ++j;
            if (j != clause.end()) {
                std::cout << " \\/ ";
            }
        }
        std::cout << ")";
        ++i;
        if (i != clauses.end()) {
            std::cout << " /\\ ";
        }
    }
    std::cout << std::endl;

    TCDCLSolver solver{};

    solver.Solve(cnfs);

    return 0;
}
