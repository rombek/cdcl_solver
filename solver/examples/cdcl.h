#pragma once

#include <vector>
#include <unordered_set>

#include <sat/cnf_clause_list.h>

using namespace std;

enum RESULT {
    UNK,
    UNSAT,
    SAT
};

class TCDCLSolver {

public:
    TCDCLSolver() = default;

    bool Solve(const cnf_clause_listt& cnf_formula);

private:
    int ChooseFreeVariable();

    RESULT UnitPropagation();

    int AnalyzeConflict();

    bvt Resolve(const bvt& input_clause, literalt literal);

    int BackTrack();


private:
    int DecisionLevel;
    int ConfilctClauseIdx;
    cnf_clause_listt Formula;
    unordered_set<int> UnusedVariables;
    vector<int> VariableNextValues;
    vector<int> VariableValues;
    vector<int> VariableDecisionLevel;
    vector<int> VariableAntecedent;
};