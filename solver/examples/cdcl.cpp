#include "cdcl.h"

#include <algorithm>
#include <iterator>
#include <cassert>

void PrintClause(const bvt& clause) {
    std::cerr << "(";
    for (bvt::const_iterator j = clause.begin(); j != clause.end();) {
        // write the literal
        literalt l = *j;
        int number = l.dimacs();
        if (number < 0) {
            std::cerr << "~";
            number = -number;
        }
        std::cerr << "x" << number;
        ++j;
        if (j != clause.end()) {
            std::cerr << " \\/ ";
        }
    }
    std::cerr << ")";
}

bool TCDCLSolver::Solve(const cnf_clause_listt& cnf_formula) {
    cerr << "Start solving" << endl;
    cnf_formula.copy_to(Formula);
    cerr << "lol" << endl;
    for (int i = 1; i <= Formula.no_variables(); i++) {
        UnusedVariables.insert(i);
    }
    cerr << "UnusedVariables.size() = " << UnusedVariables.size();
    DecisionLevel = 0;
    VariableNextValues.assign(UnusedVariables.size() + 1, 0);
    VariableValues.assign(UnusedVariables.size() + 1, -1);
    VariableDecisionLevel.assign(UnusedVariables.size() + 1, -1);
    VariableAntecedent.assign(UnusedVariables.size() + 1, -1);


    cerr << "Unused Variables = " << UnusedVariables.size() << endl;

    while (!UnusedVariables.empty()) {
        cerr << "cycle" << endl;
        DecisionLevel += 1;
        int next_var = *UnusedVariables.begin();
        cerr << "next var = " << next_var << endl;
        // Value already was true and false?
        assert(VariableNextValues[next_var] != 2);


        VariableValues[next_var] = VariableNextValues[next_var];
        cerr << "Variable " << next_var << " set with value = " << VariableValues[next_var] << endl;
        VariableDecisionLevel[next_var] = DecisionLevel;
        VariableNextValues[next_var] += 1;

        UnusedVariables.erase(next_var);

        while (true) {
            cerr << "Making unit propagation" << endl;
            const auto& result = UnitPropagation();
            if (result == RESULT::UNSAT) {
                if (DecisionLevel == 0) {
                    cout << "UNSAT" << endl;
                    return false;
                }
                cerr << "making backtrack" << endl;
                DecisionLevel = BackTrack();
                cerr << "backtracked to level = " << DecisionLevel << endl;
            } else {
                break;
            }
        }
    }

    cerr << "-----------------------------" << endl;
    cout << "SAT" << endl;
    for (int i = 1; i <= Formula.no_variables(); i++) {
        cout << "x" << i << " = " << VariableValues[i] << endl;
    }

    return true;
}

RESULT TCDCLSolver::UnitPropagation() {
    auto clauses = Formula.get_clauses();
    int idx = -1;
    bool foundUnitClause = false;
    do {
        cerr << "Unit propagation forward pass" << endl;
        foundUnitClause = false;
        idx = -1;
        for (auto& clause: clauses) {
            idx++;
            cerr << "Clause #" << idx << " = ";
            PrintClause(clause);
            cerr << endl;
            int unsetVars = 0;
            int unsetVarNo = -1;
            int unsetVarSign = -1;
            int falseCount = 0;
            bool isClauseSat = false;
            for (const auto& literal: clause) {
                int number = literal.var_no();
                cerr << "Looking for x" << number << " value = " << VariableValues[number] << endl;
                if (VariableValues[number] == -1) {
                    unsetVarNo = number;
                    unsetVarSign = literal.sign();
                    unsetVars++;
                } else if (VariableValues[number] == 1 && literal.dimacs() < 0) {
                    falseCount++;
                } else if (VariableValues[number] == 0 && literal.dimacs() > 0) {
                    falseCount++;
                } else {
                    isClauseSat = true;
                    break;
                }
            }
            if (isClauseSat) {
                continue;
            }
            if (unsetVars == 1) {
                cerr << "Unit clause!" << endl;
                cerr << "Set variable with number = " << unsetVarNo << " value = " << (unsetVarSign ^ 1) << endl;
                foundUnitClause = true;
                VariableValues[unsetVarNo] = unsetVarSign ^ 1;
                VariableDecisionLevel[unsetVarNo] = DecisionLevel;
                VariableAntecedent[unsetVarNo] = idx;
                UnusedVariables.erase(unsetVarNo);
            } else if (falseCount == clause.size()) {
                cerr << "False clause with number = " << idx << endl;
                ConfilctClauseIdx = idx;
                return RESULT::UNSAT;
            }
        }
    } while (foundUnitClause);
    return RESULT::UNK;
}

int TCDCLSolver::BackTrack() {
    cerr << "starting backtrack" << endl;
    auto clasuesIt = Formula.get_clauses().begin();
    std::advance(clasuesIt, ConfilctClauseIdx);
    auto conflictClause = *clasuesIt;
    cerr << "conflictClause = ";
    PrintClause(conflictClause);
    cerr << endl;
    cerr << "conflictDecisionLevel = " << DecisionLevel << endl;
    auto conflictDecisionLevel = DecisionLevel;
    literalt resolverLiteral;
    while (true) {
        int count = 0;
        for (const auto& literal: conflictClause) {
            int number = literal.var_no();
            if (VariableDecisionLevel[number] == conflictDecisionLevel) {
                count += 1;
            }
            if (VariableDecisionLevel[number] == conflictDecisionLevel &&
                VariableAntecedent[number] != -1) {
                resolverLiteral = literal;
            }
        }
        if (count == 1) {
            break;
        }
        conflictClause = Resolve(conflictClause, resolverLiteral);
    }
    cerr << "Add new clause = ";
    PrintClause(conflictClause);
    cerr << " to formula" << endl;
    Formula.lcnf(conflictClause);
    cerr << "result formula = " << endl;
    for (const auto clause: Formula.get_clauses()) {
        PrintClause(clause);
        cerr << endl;
    }
    int backtrackDecisionLevel = 0;
    for (const auto& literal: conflictClause) {
        int number = literal.var_no();
        auto literalDecisionLevel = VariableDecisionLevel[number];
        if (literalDecisionLevel != conflictDecisionLevel &&
            literalDecisionLevel > backtrackDecisionLevel) {
            backtrackDecisionLevel = literalDecisionLevel;
        }
    }
    cerr << "backtracking to level = " << backtrackDecisionLevel << endl;
    for (int i = 1; i <= Formula.no_variables(); i++) {
        if (VariableDecisionLevel[i] > backtrackDecisionLevel) {
            UnusedVariables.insert(i);
            VariableValues[i] = -1;
            VariableNextValues[i] -= 1;
            VariableDecisionLevel[i] = -1;
            VariableAntecedent[i] = -1;
        }
    }

    return backtrackDecisionLevel;
}

bvt TCDCLSolver::Resolve(const bvt& clause, literalt literal) {
    cerr << "resolvingg for clause1 = "; PrintClause(clause); cerr << " and literal = " << literal.dimacs() << endl;

    auto firstClause = clause;
    int badClauseIdx = VariableAntecedent[literal.var_no()];
    auto clasuesIt = Formula.get_clauses().begin();
    std::advance(clasuesIt, badClauseIdx);
    auto secondClause = *clasuesIt;
    cerr << "secondClause = "; PrintClause(secondClause); cerr << endl;

    firstClause.insert(firstClause.end(), secondClause.begin(), secondClause.end());
    sort(firstClause.begin(), firstClause.end());
    firstClause.erase(unique(firstClause.begin(), firstClause.end()), firstClause.end());

    auto it = std::find(firstClause.begin(), firstClause.end(), literal);
    if (it != firstClause.end()) {
        firstClause.erase(it);
    }
    it = std::find(firstClause.begin(), firstClause.end(), literal.negation());
    if (it != firstClause.end()) {
        firstClause.erase(it);
    }

    cerr << "resultClause = "; PrintClause(firstClause); cerr << endl;
    return firstClause; // return the result
}

